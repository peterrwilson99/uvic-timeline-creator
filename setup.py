from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()
with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(
    name="uvic-timetable-builder",
    version="1.0",
    description="Timetable built in python used for creating custom terms of classes",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Peter Wilson",
    author_email="peterrwilson99@gmail.com",
    url="www.google.ca", # i have no idea for a website lol
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
)
