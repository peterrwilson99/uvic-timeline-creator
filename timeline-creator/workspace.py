#!/usr/bin/env python3
import sys
import shutil
import os

from term import term
from student import student
from course import course

from openpyxl import load_workbook
from ipysheet import from_dataframe, to_dataframe
import pandas as pd

"""
This is here as a workspace to work with the functions made in other py files within
this directory (folder), the courses are imported off of 'Sheet5' on the the excel
file. First course objects are made, then the student object to import the course objects
into, then term objects.
"""

# TODO: Does not work for or statements on the planning sheet, excel sheet needs to
# be manually edited to choose between the options of 'ors'

# TODO: Does not account for importance of when only offered in one term
# maybe achieve above note by keeping record of how many terms of each student has left

# NOTE: To force a class into a specific semester, input into the initalization of the term
# then set num_classes to wanted (# class wanted - # classes forced)


def importCourses(excel_file,sheet_name):
    """
    imports data from excel file path
    """
    df = pd.read_excel(excel_file,sheet_name=sheet_name)
    raw_courses = df.values.tolist()
    courses = {}
    for name, offer_times, prereqs, completed in raw_courses:
        name = name.replace(' ','')
        ors = False
        if 'or' in prereqs:
            print('Not implemented yet')
        elif 'None' in prereqs:
            Prerequisites = []
        else:
            Prerequisites = prereqs.replace(' ','').split(',')
        courses[name]=course(name, offerTimes=offer_times, prereqs=Prerequisites,completed=completed)

    return courses

def main():
    """
    Given workspace to mess with code lol
    """

    excel_file = 'Courses.xlsx'
    sheet_name = 'PyInput'
    courses = importCourses(excel_file,sheet_name)

    Peter = student('Peter',courses=courses)

    Spring2022 = term('Sp', courses={})
    Spring2022.autoBuildTerm(Peter)

    Summer2022 = term('Su', courses={'SENG499':courses['SENG499']})
    Summer2022.autoBuildTerm(Peter)

    Fall2022 = term('F', courses={})
    Fall2022.autoBuildTerm(Peter)

    Spring2023 = term('Sp', courses={})
    Spring2023.autoBuildTerm(Peter)

    Peter.printStudent()
    print(Peter.todo)









if __name__ == "__main__":
    main()
