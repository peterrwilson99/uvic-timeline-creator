#!/usr/bin/env python3
import sys
import term
import student


class course:
    """
    The course class represents a singular course and its pertinent information, such
    as offer times, prerequisites, and if it has been completed.
    """
    def __init__(
        self, name: str, offerTimes = [], prereqs=[], completed=False, importance=0
    ):
        """
        :param name: The name of the course (with no spaces)
        :param offerTimes: A list containing strings of which terms the course is offered (ex. ['Sp','Su'])
        :param prereqs: A list of course names that are prerequisites
        :param completed: Boolean of if the course is Completed
        :param importance: integer value of importance of course (how many classes require this class, calculated later)
        :returns: None
        """
        self.name = name
        self.prereqs = prereqs
        self.completed = completed
        self.offerTimes = offerTimes
        self.importance = importance

    def takeCourseBool(self, student, term):
        """
        :param student: student object from student.py
        :param term: term the course wants to be taken (ex 'Sp')
        :returns: Whether the student can take this course that term
        """
        if self.completed == False:
            for course in self.prereqs:
                if course not in student.completed.keys():
                    #print("Student needs to complete", course)
                    return False
            if term not in self.offerTimes:
                #print("Course not offered in", term)
                return False
            return True
        return False

    def setCompleted(self, student):
        """
        Sets course as completed
        :param student: student object from student.py
        :returns: None
        """
        self.completed = True
        student.updateCompleted()

    def setImportance(self, value):
        """
        Sets importance of course
        :param value: Integer value # classes that require this course
        :returns: None
        """
        self.importance = value

    def printPrereqs(self):
        """
        Prints prereqs
        :param: None
        :returns: None
        """
        print("Prerequisites:")
        for course in self.prereqs:
            course.printCourse()

    def printCourse(self):
        """
        Prints course information
        :param: None
        :returns: None
        """
        s = f'{self.name}:\nPreRequisites = {str(self.prereqs)}\nOffertimes = {str(self.offerTimes)}\nCompleted = {str(self.completed)}\nImportance = {str(self.importance)}'
        print(s)
