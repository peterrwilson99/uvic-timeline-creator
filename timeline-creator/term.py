#!/usr/bin/env python3
import sys
import course
import student



class term:
    """
    term class representing each term (ex. 'Sp' or 'Su') that a student may want to
    take classes
    """
    def __init__(self, name: str, num_classes=5, courses={}):
        """
        :param name: The name of the term (ex 'Sp')
        :param num_classes: The amount of classes wanted to be taken that term
        :param courses: Dictionary containing courses, with keys set to the name of the course (ex. courses['SENG265'])
        :returns: None
        """
        self.name = name
        self.num_classes = num_classes
        self.courses = courses

    def autoBuildTerm(self, student):
        """
        Builds a term of classes from a student object with the highest importance
        courses as priority.
        :param student: Student object from student.py
        :returns: None
        """
        possible_courses = []

        for title, course in student.todo.items():
            if course.takeCourseBool(student, self.name):
                possible_courses.append(course)
        possible_courses = sortClasses(possible_courses)

        if len(possible_courses)<self.num_classes: #gets the smaller of the two to ensure correct # of courses
            length =len(possible_courses)
        else:
            length = self.num_classes
        for y in range(length):
            curCourse = possible_courses.pop(0)
            self.courses[curCourse.name]=curCourse
        #printing term
        self.printCourses()
        self.updateCourses(student)

    def updateCourses(self, student):
        """
        After the term is built, this updates the student object to set the Courses
        as completed.
        :param student: Student object from student.py
        :returns: None
        """
        for title, course in self.courses.items():
            student.courses[title].setCompleted(student) #NOTE: setcompleted should probably live in student not courses

    def addClasses(self, courses, student):
        """
        Overwrites courses in term and sets to the new courses passed in argument
        :param courses: Dictionary containing course objects
        :param student: Student object from student.py
        :returns: None
        """
        self.courses=courses
        self.updateCourses(student)
        self.printCourses()

    def printCourses(self):
        """
        Prints courses from the term
        :param: None
        :returns: None
        """
        if self.name == 'Sp':
            curTerm = 'Spring'
        elif self.name == 'Su':
            curTerm = 'Summer'
        else:
            curTerm = 'Fall'
        print("Courses in",curTerm,"Semester:")
        for title, course in self.courses.items():
            print(title)
        print()


def sortClasses(possible_courses):
    """
    Function used in 'autoBuildTerm()' to sort courses based on importance
    :param possible_courses: List of course objects
    :returns: Sorted list based on importance
    """
    sorted = [None]*len(possible_courses)
    values = []
    for course in possible_courses:
        values.append(course.importance)
    values.sort()
    values.reverse()
    for val in values:
        for course in list(possible_courses):
            if val == course.importance:
                sorted[values.index(val)] = course
                values[values.index(val)] = None
                possible_courses.remove(course)
                break
    return sorted
