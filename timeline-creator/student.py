#!/usr/bin/env python3
import sys
import course
import term

"""
Class describing a student. A student contains the courses required by their
program, the completed courses, and the ones to complete. Each course is a course
object described by course.py. The courses in the student are stored in dictionaries.
If you wanted to view the course 'SENG265' you would enter courses['SENG265'], which
will return the course object.
"""

class student:
    """
    Class describing a student. A student contains the courses required by their
    program, the completed courses, and the ones to complete. Each course is a course
    object described by course.py. The courses in the student are stored in dictionaries.
    If you wanted to view the course 'SENG265' you would enter courses['SENG265'], which
    will return the course object.
    """
    def __init__(self, name: str, courses={}, completed={}, todo={}):
        """
        :param name: Name of student (honestly worthless and never used)
        :param courses: A dictionary of course (ex. courses['SENG265'] will return the course object for SENG265)
        :param completed: Dictionary of completed courses (probably doesnt need to be a dictionary but is currently)
        :param todo: Dictionary of courses that have not been completed (also probably doesnt need to be a dictionary but currently is)
        :returns: None
        """
        self.name = name
        self.courses = courses
        self.completed = completed
        self.todo = todo
        self.createCourseLists()
        self.courseValues()
        self.updateCompleted()


    def createCourseLists(self):
        """
        After student is passed in with dict of courses this function sorts to
        completed and todo courses
        :returns: None
        """
        for title, course in self.courses.items():
            if course.completed == True:
                self.completed[title]=course
            else:
                self.todo[title]=course

    def updateCompleted(self):
        """
        Updates students completed courses
        :param: None
        :returns: None
        """
        for title, course in list(self.todo.items()):
            if course.completed == True:
                self.completed[title] = course
                self.todo.pop(title)
        self.courses = {}
        for title, course in self.todo.items():
            self.courses[title] = course
        for title, course in self.completed.items():
            self.courses[title] = course

    def courseValues(self):
        """
        Sets importance for each course depending on this students program (ie the
        course they are in)
        :param: None
        :returns: None
        """
        # finds importance of Courses
        # ie. how many classes require that class
        importance = {}
        for title, course in self.courses.items(): # loop generates a dict with the num of times a course title is references amongst all prereq lists
            for prereq in course.prereqs:
                if ',' in prereq: #this is for the 'or' option, this if statement will need to be reworked
                    for prereq_1 in prereq.split(','):
                        try:
                            importance[prereq_1] += 1
                        except KeyError:
                            importance[prereq_1] = 1
                else:
                    try:
                        importance[prereq] += 1
                    except KeyError:
                        importance[prereq] = 1
        for title, course in self.courses.items():
            try:
                self.courses[title].importance = importance[title]
            except KeyError:
                self.courses[title].importance = 0

    def getPrereqs(self, courseName):
        """
        Gets prequisites of courses
        :param courseName: name of course you want prereqs from
        :returns: list of prerequisites titles
        """
        prereqs = []
        prereqs += self.courses[courseName].prereqs
        for course in prereqs:
            if ',' in course: #this is for the 'or' option, this if statement will need to be reworked
                for course_1 in course.split(','):
                    prereqs += self.courses[course_1].prereqs
            else:
                prereqs += self.courses[course].prereqs
        return prereqs

    def printStudent(self):
        """
        Prints student information
        :param: None
        :returns: None
        """
        print(self.name)
        print("Completed: "+str(len(self.completed)))
        print("Todo: "+str(len(self.todo)))
        print("Courses: "+str(len(self.courses)))
        print()
