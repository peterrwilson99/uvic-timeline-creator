*********************************
Timeline Creator documentation
*********************************

Timeline Creator: Timeline Creator built in python3 for UVIC students with a CSC or ENG background

.. toctree::
	:maxdepth: 2
	:caption: Introduction
	:glob:
	:hidden:

	timetableCreator

.. toctree::
	:maxdepth: 2
	:caption: Modules
	:glob:
	:hidden:

	modules/*
