UVIC Timetable Creator
======================

First define courses in excel file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The excel file contains specified courses with pertinent information for
code success, define code as needed for your program. We then import
necessary packages.

.. code:: ipython3

    import sys
    import shutil
    import os
    
    from term import term
    from student import student
    from course import course
    
    from openpyxl import load_workbook
    from ipysheet import from_dataframe, to_dataframe
    import pandas as pd

Define importCourses function then import courses from excel sheet

.. code:: ipython3

    def importCourses(excel_file,sheet_name):
        """
        imports data from excel file path
        """
        df = pd.read_excel(excel_file,sheet_name=sheet_name)
        raw_courses = df.values.tolist()
        courses = {}
        for name, offer_times, prereqs, completed in raw_courses:
            name = name.replace(' ','')
            ors = False
            if 'or' in prereqs:
                print('Not implemented yet')
            elif 'None' in prereqs:
                Prerequisites = []
            else:
                Prerequisites = prereqs.replace(' ','').split(',')
            courses[name]=course(name, offerTimes=offer_times, prereqs=Prerequisites,completed=completed)
    
        return courses

.. code:: ipython3

    excel_file = 'Courses.xlsx'
    sheet_name = 'Sheet5'
    courses = importCourses(excel_file,sheet_name)

Create instance of student and import courses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use the ‘printStudent()’ function to view details about that object

.. code:: ipython3

    Peter = student('Peter',courses=courses)
    Peter.printStudent()


.. parsed-literal::

    Peter
    Completed: 24
    Todo: 14
    Courses: 38
    


Finally, create term instances
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

in first case we will add one class using ‘addClasses()’

.. code:: ipython3

    Summer = term('Su',num_classes=1, courses={})
    Summer.addClasses({'SENG275':courses['SENG275']}, Peter)


.. parsed-literal::

    Courses in Summer Semester:
    SENG275
    


In the following terms we will use ‘autoBuildTerm()’
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    Fall = term('F',num_classes=5, courses={'SENG360':courses['SENG360']})
    Fall.autoBuildTerm(Peter)


.. parsed-literal::

    Courses in Fall Semester:
    SENG360
    SENG321
    CSC360
    CSC370
    SENG350
    ECE360
    


.. code:: ipython3

    Spring = term('Sp',num_classes=5, courses={})
    Spring.autoBuildTerm(Peter)


.. parsed-literal::

    Courses in Spring Semester:
    SENG371
    CSC320
    ECE455/CSC460
    SENG401
    


.. code:: ipython3

    Summer2 = term('Su',num_classes=5, courses={})
    Summer2.autoBuildTerm(Peter)


.. parsed-literal::

    Courses in Summer Semester:
    SENG426
    SENG440
    SENG499
    


Finally, we print the student to view details
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    Peter.printStudent()


.. parsed-literal::

    Peter
    Completed: 38
    Todo: 0
    Courses: 38
    

